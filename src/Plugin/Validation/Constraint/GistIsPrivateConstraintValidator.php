<?php

namespace Drupal\media_entity_gist\Plugin\Validation\Constraint;

use Github\Exception\RuntimeException;
use Github\Client;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\media_entity_gist\Plugin\media\Source\Gist;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Validates the GistIsPrivate constraint.
 */
class GistIsPrivateConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \Github\Client
   */
  protected $client;

  /**
   * Constructs a new GistVisibleConstraintValidator.
   *
   * @param \Github\Client $http_client
   *   The http client service.
   */
  public function __construct(Client $http_client) {
    $this->client = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('media_entity_gist.php_github_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $data = '';

    if (is_string($value)) {
      $data = $value;
    }
    elseif ($value instanceof FieldItemList) {
      $data = $value->getValue()[0]['value'];
    }
    elseif ($value instanceof FieldItemInterface) {
      $class = get_class($value);
      $property = $class::mainPropertyName();
      if ($property) {
        $data = $value->{$property};
      }
    }

    $matches = [];

    if (preg_match(Gist::$regex, $data, $item_matches)) {
      $matches = $item_matches;
    }

    if (!$matches['id'] || !$matches['user']) {
      $this->context->addViolation($constraint->message);
      return;
    }
    try {
      $this->client->api('gist')->show($matches['id']);
    }
    catch (RuntimeException $ex) {
      $this->context->addViolation($constraint->notFoundMessage);
    }
  }

}
