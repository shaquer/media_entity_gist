<?php

namespace Drupal\media_entity_gist\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media_entity_gist\Plugin\media\Source\Gist;

/**
 * Plugin implementation of the 'media_entity_gist_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "media_entity_gist_formatter",
 *   label = @Translation("Gist"),
 *   field_types = {
 *     "media_entity_gist_field"
 *   }
 * )
 */
class GistFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $matches = [];

      if (!preg_match(Gist::$regex, $item->value, $matches)) {
        return ['#markup' => $this->t('Not a valid gist url')];
      }

      $query = '';
      $parse = UrlHelper::parse($item->value);

      if (isset($parse['query']) && isset($parse['query']['file'])) {
        $query = '?file=' . $parse['query']['file'];
      }

      if (!empty($matches['user']) && !empty($matches['id'])) {
        $element[$delta] = [
          '#theme' => 'media_entity_gist_file',
          '#uri' => "https://gist.github.com/" . $matches['user'] . "/" . $matches['id'] . ".js${query}",
        ];
      }
    }
    return $element;
  }

}
