<?php

namespace Drupal\media_entity_gist\Plugin\media\Source;

use Github\Exception\RuntimeException;
use Github\Client;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Media source plugin.
 *
 * @MediaSource(
 *   id = "gist",
 *   label = @Translation("Github gist"),
 *   allowed_field_types = {"media_entity_gist_field"},
 *   default_thumbnail_filename = "github.png",
 *   description = @Translation("Provides business logic and metadata for gists."),
 * )
 */
class Gist extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {
  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The git fetcher.
   *
   * @var \Github\Client
   */
  protected $client;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  public static $regex = '@((http|https):)//(gist\.)github\.com/(?<user>[a-z0-9_-]+)/(?<id>[a-z0-9]+)@i';

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   Config field type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Github\Client $git_fetcher
   *   The git fetcher.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    FieldTypePluginManagerInterface $field_type_manager,
    ConfigFactoryInterface $config_factory,
    RendererInterface $renderer,
    Client $git_fetcher,
    LoggerChannelInterface $logger
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->renderer = $renderer;
    $this->client = $git_fetcher;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('renderer'),
      $container->get('media_entity_gist.php_github_api'),
      $container->get('logger.factory')->get('media_entity_gist')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      'id' => $this->t('Gist ID'),
      'user' => $this->t('Gist user information'),
      'forks_url' => $this->t('Gist fork url'),
      'created_at' => $this->t('Created at'),
      'updated_at' => $this->t('updated at'),
      'comment_count' => $this->t('Comment count'),
      'description' => $this->t('Description'),
      'avatar_url' => $this->t('Avatar url'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $matches = $this->matchRegexp($media);

    if (!$matches['id']) {
      return NULL;
    }

    $response = NULL;
    try {
      $response = $this->client->api('gist')->show($matches['id']);
    }
    catch (RuntimeException $ex) {
      return NULL;
    }

    // First we return the fields that are available from regex.
    switch ($attribute_name) {
      case 'id':
        return $matches['id'];

      case 'user':
        return $matches['user'] ?: NULL;

      case 'avatar_url':
        return isset($response['owner']['avatar_url']) ? $response['owner']['avatar_url'] : NULL;
    }

    if (isset($response[$attribute_name])) {
      return $response[$attribute_name];
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)->set('label', 'Gist URL');
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return [
      'GistIsPrivate' => [],
    ];
  }

  /**
   * Runs preg_match on embed code/URL.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media object.
   *
   * @return array|bool
   *   Array of preg matches or FALSE if no match.
   *
   * @see preg_match()
   */
  protected function matchRegexp(MediaInterface $media) {
    $matches = [];

    $source_field = $this->getSourceFieldDefinition($media->bundle->entity)->getName();
    if ($media->hasField($source_field)) {
      $property_name = $media->get($source_field)->first()->mainPropertyName();

      if (preg_match(static::$regex, $media->get($source_field)->{$property_name}, $matches)) {
        return $matches;
      }

    }

    return FALSE;
  }

}
